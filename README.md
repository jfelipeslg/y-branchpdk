# Y-Branch

## Descrição

Projeto de desenvolvimento do equipamento fotônico **Y-Branch**.

O equipamento foi construído com base no estudo [1] para atuar na banda C (de 1530 nm a 1565 nm) no modo TE. Após o dispositivo ser projetado para atuar na banda C, é feita a transição para a banda O (de 1260 nm a 1360 nm). Entretanto, devido à má performance, é proposto uma otimização para melhorar a transmissão do equipamento. 

As simulações são produzidas no pacote Ansys-Lumerical.

## Instruções

Pastas do repositório:
- "Banda C" - Equipamento construído para atuar na banda C;
- "Banda O/Não otimizado" - Transição do dispositivo para banda O;
- "Banda O/Otimizado" - Dispositivo otimizado para atuar na banda O.

As pastas contém:
- "YBranchPDK_Profile" - arquivo .gds do equipamento;
- "YBranchPDK_Import" - arquivo .lsf de importação e parametrização do equipamento;
- "YBranchPDK_T-Graphs" - arquivo .lsf para gerar os gráficos da transmissão e reflexão do equipamento;
- "YBranchPDK_NoRun" - arquivo .fsp de simulação no FDTD (Finite Difference Time Domain);
- "YBranchPDK_S-Par" - arquivo .dat de parâmetros S (modelo compacto para circuitos fotônicos).


## Referências

[1] Zhang, Yi, et al. "A compact and low loss Y-junction for submicron silicon 
waveguide." Optics express 21.1 (2013): 1310-1316
